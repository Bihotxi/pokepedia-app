# Pokepedia

Pokepedia is an application that provides information about Pokemons.

## Installation

To use Pokepedia, follow these steps:

1. Clone the repository: `git clone https://gitlab.com/Bihotxi/pokepedia-app.git`
2. Navigate to the project directory.
3. Install the required dependencies: `npm install`
4. Start the application: `npm start`

## Usage

Once you have installed and started Pokepedia, you can use it to search for information about different Pokemons. Simply enter the name of the Pokemon you are interested in into the search bar and hit enter. The application will display information about the Pokemon, including its type, abilities, etc.

## Credits

Pokepedia was created by Roberto Palomino. The application uses data from the [PokeAPI](https://pokeapi.co/).

describe("Pokepedia home page", () => {
  it("should show the home page", () => {
    cy.visit("localhost:8080");
    cy.get(".pokemon__header").should("be.visible");
    cy.get("h1 > a").should("be.visible");
    cy.get(".pokemon-card__list").should("be.visible");
    cy.get("section").should("be.visible");
    cy.get(".search-input").should("be.visible");
  });

  it("should show a selected pokemon details", () => {
    cy.visit("localhost:8080");
    cy.get(
      ":nth-child(1) > .pokemon-card__link > pokemon-card-ui > .pokemon-card"
    ).click();
    cy.get(".pokemon__details-container").should("be.visible");
  });

  it("should search the name of a specific pokemon successfully", () => {
    cy.visit("localhost:8080");
    cy.get(".search-input").type("bulbasaur");
    cy.get(".pokemon-card__list").contains("bulbasaur").click();
  });

  it("should search the name of a specific pokemon without success", () => {
    cy.visit("localhost:8080");
    cy.get(".search-input").type("bulbasaur");
    cy.get(".pokemon__details-container").should("not.exist");
    cy.get(".pokemon__header").should("be.visible");
    cy.get("h1 > a").should("be.visible");
  });

  it("should return to home page", () => {
    cy.visit("localhost:8080");
    cy.get(
      ":nth-child(1) > .pokemon-card__link > pokemon-card-ui > .pokemon-card"
    ).click();
    cy.get(".pokemon__details-container").should("be.visible");
    cy.get("a").click();
    cy.visit("localhost:8080");
  });
});

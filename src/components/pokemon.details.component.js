import { LitElement, html } from "lit";
import { PokemonsDetailsUseCase } from "../usecases/pokemons-details.usecase";
import { router } from "../main";
import "../ui/pokemon-details.ui";

export class PokemonDetailsComponent extends LitElement {
  static get properties() {
    return {
      pokemon: {
        type: Object,
      },
    };
  }
  async connectedCallback() {
    super.connectedCallback();

    const currentPokemonId = router.location.params.id || "";

    this.pokemon = await PokemonsDetailsUseCase.execute(currentPokemonId);
  }

  render() {
    return html`
      <pokemon-details-ui .pokemon="${this.pokemon}"></pokemon-details-ui>
    `;
  }
  createRenderRoot() {
    return this;
  }
}

customElements.define("pokemon-details-component", PokemonDetailsComponent);

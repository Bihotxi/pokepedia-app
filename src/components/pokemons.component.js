import { LitElement, html } from "lit";
import { AllPokemonsUseCase } from "../usecases/all-pokemons.usecase";
import { SearchPokemonsUseCase } from "../usecases/search-pokemons.usecase";
import "../ui/pokemon-card.ui";
import "../ui/pokemon-search.ui";

export class PokemonsComponent extends LitElement {
  static get properties() {
    return {
      filteredPokemons: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();

    this.pokemon = await AllPokemonsUseCase.execute();
    this.filteredPokemons = this.pokemon;
  }

  render() {
    const showResults =
      this.filteredPokemons && this.filteredPokemons.length > 0;

    return html`
      <pokemon-search-ui @input="${this.handleSearchInput}">
      </pokemon-search-ui>
      <div class="pokemon-card__container">
        ${showResults
          ? html`<ul class="pokemon-card__list">
              ${this.filteredPokemons.map(
                (pokemon) => html`
                  <li>
                    <a
                      href="${`./details/${pokemon.id}`}"
                      class="pokemon-card__link">
                      <pokemon-card-ui .pokemon="${pokemon}"></pokemon-card-ui>
                    </a>
                  </li>
                `
              )}
            </ul>`
          : ""}
      </div>
    `;
  }
  createRenderRoot() {
    return this;
  }

  handleSearchInput(event) {
    const searchValue = event.target.value;
    this.filteredPokemons = SearchPokemonsUseCase.execute(
      this.pokemon,
      searchValue
    );
  }
}

customElements.define("pokemons-component", PokemonsComponent);

import { Router } from "@vaadin/router";
import "./pages/home.page";
import "./pages/details.page";
import "./main.css";

const outlet = document.querySelector("#outlet");
export const router = new Router(outlet);

router.setRoutes([
  { path: "/", component: "home-page" },
  { path: "/details/:id", component: "details-page" },
  { path: "(.*)", redirect: "/" },
]);

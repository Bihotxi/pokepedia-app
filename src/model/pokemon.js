export class Pokemon {
  constructor({
    id,
    abilities,
    types,
    name,
    height,
    weight,
    image,
    color,
    shape,
  }) {
    (this.id = id),
      (this.abilities = abilities),
      (this.types = types),
      (this.name = name),
      (this.height = height),
      (this.weight = weight),
      (this.image = image),
      (this.color = color),
      (this.shape = shape);
  }
}

import "../components/pokemon.details.component";
import "../ui/pokemon-header.ui";

export class DetailsPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `<pokemon-header-ui></pokemon-header-ui><pokemon-details-component class="pokemon__details"></pokemon-details-component>
      `;
  }
}
customElements.define("details-page", DetailsPage);

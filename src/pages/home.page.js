import "../components/pokemons.component";
import "../ui/pokemon-header.ui";

export class HomePage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `<pokemon-header-ui></pokemon-header-ui>
    <section ><pokemons-component ></pokemons-component></section>
   `;
  }
}
customElements.define("home-page", HomePage);

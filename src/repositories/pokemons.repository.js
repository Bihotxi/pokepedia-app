import axios from "axios";

export class PokemonsRepository {
  async getAllPokemons() {
    const initialPokemons = (
      await axios
        .get("https://pokeapi.co/api/v2/pokemon?limit=35")
        .catch((error) =>
          console.log("Error trying to get all pokemons: ", error)
        )
    ).data;

    const pokemons = await Promise.all(
      initialPokemons.results.map(async (pokemon) => {
        const pokemonData = (
          await axios
            .get(pokemon.url)
            .catch((error) =>
              console.log(
                "Error trying to get one pokemon in all pokemons query: ",
                error
              )
            )
        ).data;

        return {
          id: pokemonData.id,
          name: pokemonData.name,
          image: pokemonData.sprites.other.dream_world.front_default,
          height: pokemonData.height,
          weight: pokemonData.weight,
          type: pokemonData.types[0].type.name,
        };
      })
    );

    return { ...initialPokemons, results: pokemons };
  }

  async getPokemonsDetails(id) {
    const pokemon = (
      await axios
        .get(`https://pokeapi.co/api/v2/pokemon/${id}`)
        .catch((error) => console.log("Error trying to get pokemon: ", error))
    ).data;
    const pokemonsColorAndShape = (
      await axios
        .get(`https://pokeapi.co/api/v2/pokemon-species/${id}`)
        .catch((error) =>
          console.log("Error trying to get pokemon-species: ", error)
        )
    ).data;
    const pokemonsDetails = {
      id: pokemon.id,
      abilities: pokemon.abilities,
      types: pokemon.types,
      name: pokemon.name,
      height: pokemon.height,
      weight: pokemon.weight,
      image: pokemon.sprites.other.dream_world.front_default,
      color: pokemonsColorAndShape.color,
      shape: pokemonsColorAndShape.shape,
    };

    return pokemonsDetails;
  }
}

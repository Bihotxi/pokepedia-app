import { LitElement, html } from "lit";

export class PokemonCardUi extends LitElement {
  static get properties() {
    return {
      pokemon: { type: Object },
    };
  }
  render() {
    return html`
      <article class="pokemon-card">
        <header class="pokemon-card__header">
          <h2>${this.pokemon?.name}</h2>
        </header>
        <img
          src="${this.pokemon?.image}"
          alt="${this.pokemon?.name}"
          height="200" />
      </article>
    `;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("pokemon-card-ui", PokemonCardUi);

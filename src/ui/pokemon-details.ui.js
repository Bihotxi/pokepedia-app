import { LitElement, html } from "lit";

export class PokemonDetailsUi extends LitElement {
  static get properties() {
    return {
      pokemon: { type: Object },
    };
  }

  render() {
    return html`
      <section class="pokemon__details-container">
        <header>
          <span>#${this.pokemon?.id}</span>
        </header>
        <figure class="pokemon__details-container-image">
          <div></div>
          <img
            src="${this.pokemon?.image}"
            alt="${this.pokemon?.name}"
            height="300" />

          <h2 class="pokemon__details-container-name">${this.pokemon?.name}</h2>
        </figure>

        <ul class="pokemon__details-info">
          <li class="pokemon__details-info-item">
            <span class="pokemon__details-container-span">Height</span>
            <span>${this.pokemon?.height} m</span>
          </li>

          <li class="pokemon__details-info-item">
            <span class="pokemon__details-container-span">Weight</span>
            <span>${this.pokemon?.weight} kg</span>
          </li>

          <li class="pokemon__details-info-item">
            <span class="pokemon__details-container-span">Color</span>
            <span>${this.pokemon?.color}</span>
          </li>

          <li class="pokemon__details-info-item">
            <span class="pokemon__details-container-span">Shape</span>
            <span>${this.pokemon?.shape}</span>
          </li>

          <li class="pokemon__details-info-item">
            <span class="pokemon__details-container-span">Abilities</span>
            <span>${this.pokemon?.abilities}</span>
          </li>

          <li class="pokemon__details-info-item">
            <span class="pokemon__details-container-span">Types</span>
            <span>${this.pokemon?.types}</span>
          </li>
        </ul>
      </section>
    `;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("pokemon-details-ui", PokemonDetailsUi);

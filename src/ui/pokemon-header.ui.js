import { LitElement, html } from "lit";

export class PokemonHeaderhUi extends LitElement {
  render() {
    return html`
      <header class="pokemon__header">
        <h1><a href="/">Pokepedia</a></h1>
      </header>
    `;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("pokemon-header-ui", PokemonHeaderhUi);

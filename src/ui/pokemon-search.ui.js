import { LitElement, html } from "lit";

export class PokemonSearchUi extends LitElement {
  render() {
    return html` <input
      class="search-input"
      type="text"
      placeholder="Search" />`;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("pokemon-search-ui", PokemonSearchUi);

import { PokemonsRepository } from "../repositories/pokemons.repository";
import { Pokemon } from "../model/pokemon";

export class AllPokemonsUseCase {
  static async execute() {
    const repository = new PokemonsRepository();
    const pokemonsResponse = await repository.getAllPokemons();
    const pokemonResults = pokemonsResponse.results;

    const pokemons = pokemonResults.map((pokemon) => {
      return new Pokemon({
        id: pokemon.id,
        name: pokemon.name,
        height: pokemon.height,
        weight: pokemon.weight,
        image: pokemon.image,
        type: pokemon.type,
      });
    });

    return pokemons;
  }
}

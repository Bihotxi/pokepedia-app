import { PokemonsRepository } from "../repositories/pokemons.repository";
import { Pokemon } from "../model/pokemon";

export class PokemonsDetailsUseCase {
  static async execute(id) {
    const repository = new PokemonsRepository();
    const pokemon = await repository.getPokemonsDetails(id);

    const pokemonsDetails = new Pokemon({
      id: pokemon.id?.toString().padStart(4, "0"),
      abilities: pokemon.abilities
        .map((ability) => ability?.ability?.name)
        .join(", "),
      types: pokemon.types.map((type) => type.type.name).join(", "),
      name: pokemon.name,
      height: pokemon.height,
      weight: pokemon.weight,
      image: pokemon.image,
      color: pokemon.color.name,
      shape: pokemon.shape.name,
    });

    return pokemonsDetails;
  }
}

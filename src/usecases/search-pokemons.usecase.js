export class SearchPokemonsUseCase {
  static execute(pokemonList, searchValue) {
    if (!pokemonList) {
      return [];
    }

    if (!searchValue) {
      return pokemonList;
    }

    const lowercaseSearchValue = searchValue.toLowerCase();
    const filteredPokemons = pokemonList.filter((pokemon) =>
      pokemon.name.toLowerCase().startsWith(lowercaseSearchValue)
    );

    return filteredPokemons;
  }
}

import { PokemonsRepository } from "../src/repositories/pokemons.repository";
import { AllPokemonsUseCase } from "../src/usecases/all-pokemons.usecase";
import { pokemons } from "./fixtures/pokemons";

jest.mock("../src/repositories/pokemons.repository");

describe("All pokemons use case", () => {
  beforeEach(() => {
    PokemonsRepository.mockClear();
  });

  it("should get all pokemons", async () => {
    PokemonsRepository.mockImplementation(() => {
      return {
        getAllPokemons: () => {
          return {
            results: pokemons,
          };
        },
      };
    });

    const allPokemons = await AllPokemonsUseCase.execute();

    expect(typeof allPokemons === "object").toBe(true);
    expect(allPokemons.length).toBe(pokemons.length);
    expect(allPokemons[0].name).toBe(pokemons[0].name);
    expect(allPokemons[0].image).toBe(pokemons[0].image);
  });
});

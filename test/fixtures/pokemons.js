export const pokemons = [
  {
    id: 1,
    name: "bulbasaur",
    height: 7,
    weight: 69,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/1.svg",
  },
  {
    id: 2,
    name: "ivysaur",
    height: 10,
    weight: 130,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/2.svg",
  },
  {
    id: 3,
    name: "venusaur",
    height: 20,
    weight: 1000,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/3.svg",
  },
  {
    id: 4,
    name: "charmander",
    height: 6,
    weight: 85,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/4.svg",
  },
  {
    id: 5,
    name: "charmeleon",
    height: 11,
    weight: 190,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/5.svg",
  },
  {
    id: 6,
    name: "charizard",
    height: 17,
    weight: 905,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/6.svg",
  },
  {
    id: 7,
    name: "squirtle",
    height: 5,
    weight: 90,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/7.svg",
  },
  {
    id: 8,
    name: "wartortle",
    height: 10,
    weight: 225,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/8.svg",
  },
  {
    id: 9,
    name: "blastoise",
    height: 16,
    weight: 855,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/9.svg",
  },
  {
    id: 10,
    name: "caterpie",
    height: 3,
    weight: 29,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/10.svg",
  },
  {
    id: 11,
    name: "metapod",
    height: 7,
    weight: 99,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/11.svg",
  },
  {
    id: 12,
    name: "butterfree",
    height: 11,
    weight: 320,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/12.svg",
  },
  {
    id: 13,
    name: "weedle",
    height: 3,
    weight: 32,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/13.svg",
  },
  {
    id: 14,
    name: "kakuna",
    height: 6,
    weight: 100,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/14.svg",
  },
  {
    id: 15,
    name: "beedrill",
    height: 10,
    weight: 295,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/15.svg",
  },
  {
    id: 16,
    name: "pidgey",
    height: 3,
    weight: 18,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/16.svg",
  },
  {
    id: 17,
    name: "pidgeotto",
    height: 11,
    weight: 300,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/17.svg",
  },
  {
    id: 18,
    name: "pidgeot",
    height: 15,
    weight: 395,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/18.svg",
  },
  {
    id: 19,
    name: "rattata",
    height: 3,
    weight: 35,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/19.svg",
  },
  {
    id: 20,
    name: "raticate",
    height: 7,
    weight: 185,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/20.svg",
  },
  {
    id: 21,
    name: "spearow",
    height: 3,
    weight: 20,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/21.svg",
  },
  {
    id: 22,
    name: "fearow",
    height: 12,
    weight: 380,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/22.svg",
  },
  {
    id: 23,
    name: "ekans",
    height: 20,
    weight: 69,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/23.svg",
  },
  {
    id: 24,
    name: "arbok",
    height: 35,
    weight: 650,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/24.svg",
  },
  {
    id: 25,
    name: "pikachu",
    height: 4,
    weight: 60,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/25.svg",
  },
  {
    id: 26,
    name: "raichu",
    height: 8,
    weight: 300,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/26.svg",
  },
  {
    id: 27,
    name: "sandshrew",
    height: 6,
    weight: 120,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/27.svg",
  },
  {
    id: 28,
    name: "sandslash",
    height: 10,
    weight: 295,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/28.svg",
  },
  {
    id: 29,
    name: "nidoran-f",
    height: 4,
    weight: 70,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/29.svg",
  },
  {
    id: 30,
    name: "nidorina",
    height: 8,
    weight: 200,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/30.svg",
  },
  {
    id: 31,
    name: "nidoqueen",
    height: 13,
    weight: 600,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/31.svg",
  },
  {
    id: 32,
    name: "nidoran-m",
    height: 5,
    weight: 90,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/32.svg",
  },
  {
    id: 33,
    name: "nidorino",
    height: 9,
    weight: 195,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/33.svg",
  },
  {
    id: 34,
    name: "nidoking",
    height: 14,
    weight: 620,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/34.svg",
  },
  {
    id: 35,
    name: "clefairy",
    height: 6,
    weight: 75,
    image:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/35.svg",
  },
];

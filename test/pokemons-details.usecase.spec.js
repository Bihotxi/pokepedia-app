import { PokemonsRepository } from "../src/repositories/pokemons.repository";
import { PokemonsDetailsUseCase } from "../src/usecases/pokemons-details.usecase";
import { pokemonsDetailsModel } from "./fixtures/pokemons.details";

jest.mock("../src/repositories/pokemons.repository");

describe("Details pokemons use case", () => {
  beforeEach(() => {
    PokemonsRepository.mockClear();
  });

  it("should show pokemons details", async () => {
    const pokemonId = 1;
    PokemonsRepository.mockImplementation(() => {
      return {
        getPokemonsDetails: () => {
          return pokemonsDetailsModel;
        },
      };
    });

    const pokemonsDetailsResponse = await PokemonsDetailsUseCase.execute(
      pokemonId
    );

    expect(pokemonsDetailsResponse.name).toEqual(pokemonsDetailsModel.name);
    expect(pokemonsDetailsResponse.height).toEqual(pokemonsDetailsModel.height);
    expect(pokemonsDetailsResponse.weight).toEqual(pokemonsDetailsModel.weight);
    expect(pokemonsDetailsResponse.color).toEqual(
      pokemonsDetailsModel.color.name
    );
    expect(pokemonsDetailsResponse.shape).toEqual(
      pokemonsDetailsModel.shape.name
    );

    expect(pokemonsDetailsResponse.types).toEqual(
      `${pokemonsDetailsModel.types[0].type.name}, ${pokemonsDetailsModel.types[1].type.name}`
    );
    expect(pokemonsDetailsResponse.abilities).toEqual(
      `${pokemonsDetailsModel.abilities[0].ability.name}, ${pokemonsDetailsModel.abilities[1].ability.name}`
    );
  });
});

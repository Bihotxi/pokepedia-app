import { SearchPokemonsUseCase } from "../src/usecases/search-pokemons.usecase";
import { pokemons } from "./fixtures/pokemons";

describe("Search Pokemons use case", () => {
  it("should return an empty array when no find pokemons", () => {
    const searchValue = "no-found";
    const result = SearchPokemonsUseCase.execute(pokemons, searchValue);
    expect(result).toEqual([]);
  });

  it("should show all pokemons when the search value is empty", () => {
    const searchValue = "";
    const result = SearchPokemonsUseCase.execute(pokemons, searchValue);
    expect(result).toEqual(pokemons);
  });

  it("should show the searched pokemon based on the search value", () => {
    const searchValue = "cha";
    const expectedResults = [
      {
        id: 4,
        name: "charmander",
        height: 6,
        weight: 85,
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/4.svg",
      },
      {
        id: 5,
        name: "charmeleon",
        height: 11,
        weight: 190,
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/5.svg",
      },
      {
        id: 6,
        name: "charizard",
        height: 17,
        weight: 905,
        image:
          "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/6.svg",
      },
    ];
    const result = SearchPokemonsUseCase.execute(pokemons, searchValue);
    expect(result).toEqual(expectedResults);
  });
});
